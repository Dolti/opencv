﻿#include "SledzenieKoloru.h"
/// SledzenieKoloru - tryb sledzenia koloru
int SledzenieKoloru()
{
	/// cv::VideoCapture - uruchamia domyslna kamere podlaczona do komputera
	cv::VideoCapture cap(0); 
	if (!cap.isOpened())
	{
		cout << "Nie mozna uruchomic kamery" << endl;
		return -1;
	}
	/// cv::namedWindow - nazwa nowego okna
	cv::namedWindow("Konfiguracja", CV_WINDOW_AUTOSIZE);
	/// iMinH, iMaxH ... - zakresy parametrow modelu HSV
	int iMinH = 0;
	int iMaxH = 179;
	int iMinS = 0;
	int iMaxS = 255;
	int iMinV = 0;
	int iMaxV = 255;
	/// cvCreateTrackbar - tworzy suwak
	cvCreateTrackbar("Min H", "Konfiguracja", &iMinH, 179);
	cvCreateTrackbar("Max H", "Konfiguracja", &iMaxH, 179);
	cvCreateTrackbar("Min S", "Konfiguracja", &iMinS, 255);
	cvCreateTrackbar("Max S", "Konfiguracja", &iMaxS, 255);
	cvCreateTrackbar("Min V", "Konfiguracja", &iMinV, 255);
	cvCreateTrackbar("Max V", "Konfiguracja", &iMaxV, 255);
	int iLastX = -1;
	int iLastY = -1;
	cv::Mat imgTmp;
	cap.read(imgTmp);
	cv::Mat imgLines = cv::Mat::zeros(imgTmp.size(), CV_8UC3);;
	while (true)
	{
		cv::Mat imgOriginal;
		bool bSuccess = cap.read(imgOriginal);
		if (!bSuccess)
		{
			cout << "Nie mozna odczytac klatki z kamery" << endl;
			break;
		}
		cv::Mat imgHSV;
		/// cvtColor - konwersja koloru z BGR do HSV
		cvtColor(imgOriginal, imgHSV, cv::COLOR_BGR2HSV);
		cv::Mat imgThresholded;
		/// inRange - uwzglednienie wartosci ustawionych na suwakach
		inRange(imgHSV, cv::Scalar(iMinH, iMinS, iMinV), cv::Scalar(iMaxH, iMaxS, iMaxV), imgThresholded);
		/// erode - przeprowadza erozje obrazu z uzyciem zdefiniowanego elementu strukturalnego
		erode(imgThresholded, imgThresholded, cv::getStructuringElement(cv::MORPH_ELLIPSE, cv::Size(5, 5)));
		/// dilate - przeprowadza dylatacje obrazu z uzyciem zdefiniowanego elementu strukturalnego
		dilate(imgThresholded, imgThresholded, cv::getStructuringElement(cv::MORPH_ELLIPSE, cv::Size(5, 5)));
		dilate(imgThresholded, imgThresholded, cv::getStructuringElement(cv::MORPH_ELLIPSE, cv::Size(5, 5)));
		erode(imgThresholded, imgThresholded, cv::getStructuringElement(cv::MORPH_ELLIPSE, cv::Size(5, 5)));
		/// moments - wyznacza momenty obrazu
		cv::Moments oMoments = moments(imgThresholded);
		double dM01 = oMoments.m01;
		double dM10 = oMoments.m10;
		double dArea = oMoments.m00;
		/// dArea - jezeli obszar momentow jest mniejszy niz 10000 to nic nie rob
		if (dArea > 10000)
		{
			int posX = dM10 / dArea;
			int posY = dM01 / dArea;
			if (iLastX >= 0 && iLastY >= 0 && posX >= 0 && posY >= 0)
			{
				/// line - rysowanie lini
				line(imgLines, cv::Point(posX, posY), cv::Point(iLastX, iLastY), cv::Scalar(0, 0, 255), 2);
			}
			iLastX = posX;
			iLastY = posY;
		}
		/// imshow - pokazanie obrazu wideo
		imshow("Przetworzony obraz", imgThresholded);
		imgOriginal = imgOriginal + imgLines;
		imshow("Oryginalny obraz", imgOriginal);
		if (cv::waitKey(30) == 27)
		{
			cout << "Klawisz esc zostal nacisniety przez uzytkownika" << endl;
			break;
		}
	}

	return 0;
}