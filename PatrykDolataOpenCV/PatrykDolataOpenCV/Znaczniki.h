#include "Biblioteki.h"

int Znaczniki();
void Watek();

class CJoints
{
public:
	CJoints();
	~CJoints();
	void WyswietlDaneNaEkranie(cv::Mat imgOryginalny);
	void Tic();
	void SledzenieNogi();
	void PoliczSkale();
	void Watek();
	void Watek1();
	void Watek2();
	void WystartujWatek();
	void ZapiszDaneDoPliku();
	double Toc();
	double PoliczPredkosc(cv::Point Start, cv::Point Stop, double Czas);
	double PoliczPrzyspieszenie(double dPredkoscTMP, double dPredkosc);
	bool OkreslHSVznacznikow();
	bool PodajWymiaryNogi();
	bool InitKamery();
	cv::Mat PrzetworzObraz(cv::Mat imgHSV, int iMinH, int iMaxH, int iMinS, int iMaxS, int iMinV, int iMaxV);
	cv::Point WykryjZnaczniki(cv::Mat imgPrzetworzonyObraz, cv::Point wspolrzedne);
	thread m_tWatekKostka;
	thread m_tWatekKolano;
	thread m_tWatekBiodro;

private:	
	int m_iZnacznikBiodroHSV[6];
	int m_iZnacznikKolanoHSV[6];
	int m_iZnacznikKostkaHSV[6];
	int m_iBiodroKolanoMilimetry;
	int m_iKolanoKostkaMilimetry;
	int m_iZnacznikHSV;
	int m_iZmierzone;
	double m_dPredkoscKostka;
	double m_dPredkoscKolano;
	double m_dPredkoscBiodro;
	double m_dPrzyspieszenieKostka;
	double m_dPrzyspieszenieKolano;
	double m_dPrzyspieszenieBiodro;
	double m_dDroga;
	double m_dSkala;
	std::ostringstream  m_sPomiaryKostka;
	std::ostringstream  m_sPomiaryKolano;
	std::ostringstream  m_sPomiaryBiodro;
	cv::Point m_pWspZnacznikaNaBiodrze;
	cv::Point m_pWspZnacznikaNaBiodrzeStart;
	cv::Point m_pWspZnacznikaNaBiodrzeTMP;
	cv::Point m_pWspZnacznikaNaKolanie;
	cv::Point m_pWspZnacznikaNaKolanieStart;
	cv::Point m_pWspZnacznikaNaKolanieTMP;
	cv::Point m_pWspZnacznikaNaKostce;
	cv::Point m_pWspZnacznikaNaKostceStart;
	cv::Point m_pWspZnacznikaNaKostceTMP;
	cv::VideoCapture m_wideo;
	std::stack<clock_t> tictoc_stack;
};
