#include "Biblioteki.h"
#include "WykrywanieKoloru.h"
#include "SledzenieKoloru.h"
#include "WykrywanieSkory.h"
#include "Znaczniki.h"
#include <windows.h>

/// main - petla glowna programu, wybor trybu pracy.
int main(int argc, char** argv)
{
	int iWybor = 0;
	HWND hwnd = ::GetConsoleWindow();
	::SetWindowPos(hwnd, HWND_TOPMOST, 0, 0, 0, 0, SWP_DRAWFRAME | SWP_NOMOVE | SWP_NOSIZE | SWP_SHOWWINDOW);
	::ShowWindow(hwnd, SW_NORMAL);
	cout << "Wybierz tryb:\n1. Wykrywanie koloru\n2. Sledzenie koloru\n3. Wykrywanie skory\n4. Wykrywanie jointow z wykorzystaniem znacznikow\n";
	cin >> iWybor;
	CJoints jointy;
	switch (iWybor)
	{
	case 1:
		WykrywanieKoloru();
		break;
	case 2:
		SledzenieKoloru();
		break;
	case 3:
		WykrywanieSkory();
		break;
	case 4:
		if (!jointy.OkreslHSVznacznikow())
		{
			cout << "Nieprawidlowe wartosci HSV.\n";
			break;
		}
		if (!jointy.PodajWymiaryNogi())
		{
			cout << "Nieprawidlowe parametry nogi.\n";
			break;
		}
		jointy.WystartujWatek();
		while (true)
		{
			
			jointy.SledzenieNogi();
			if (cv::waitKey(30) == 27)
			{
				cout << "Klawisz esc zostal nacisniety przez uzytkownika" << endl;
				break;
			}
		}
		jointy.ZapiszDaneDoPliku();
		break;
	default:
		cout << "Niepoprawny wybor.\nWpisz exit, aby zamknac program.\n"; 
		cin >> iWybor;
		break;
	}
	return 0;
}

