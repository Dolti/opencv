#include "WykrywanieSkory.h"
/// WykrywanieSkory - tryb wykrywania skory
int WykrywanieSkory() {
	/// cv::VideoCapture - uruchamia domyslna kamere podlaczona do komputera
	cv::VideoCapture cap(0);
	if (!cap.isOpened())
	{
		cout << "Nie mozna uruchomic kamery" << endl;
		return -1;
	}
	cv::Mat3b frame;
	while (cap.read(frame))
	{
		/// cvtColor - konwersja koloru z BGR do HSV
		cvtColor(frame, frame, CV_BGR2HSV);
		/// GaussianBlur - filtr Gaussa
		GaussianBlur(frame, frame, cv::Size(7, 7), 1, 1);
		for (int r = 0; r<frame.rows; ++r)
		{
			for (int c = 0; c<frame.cols; ++c)
				// 0<H<0.25  -   0.15<S<0.9    -    0.2<V<0.95   
				if ((frame(r, c)[0]>5) && (frame(r, c)[0] < 17) && (frame(r, c)[1]>38) && (frame(r, c)[1]<250) && (frame(r, c)[2]>51) && (frame(r, c)[2]<242));
				else for (int i = 0; i<3; ++i)	frame(r, c)[i] = 0;
		}
		cv::Mat1b frame_gray;
		cvtColor(frame, frame, CV_HSV2BGR);
		cvtColor(frame, frame_gray, CV_BGR2GRAY);
		/// threshold - zmiana obrazu ze skali szarosci na wartosci binarne
		threshold(frame_gray, frame_gray, 60, 255, CV_THRESH_BINARY);
		/// morphologyEx - operacje morfogoliczne
		morphologyEx(frame_gray, frame_gray, CV_MOP_ERODE, cv::Mat1b(3, 3, 1), cv::Point(-1, -1), 3);
		morphologyEx(frame_gray, frame_gray, CV_MOP_OPEN, cv::Mat1b(7, 7, 1), cv::Point(-1, -1), 1);
		morphologyEx(frame_gray, frame_gray, CV_MOP_CLOSE, cv::Mat1b(9, 9, 1), cv::Point(-1, -1), 1);
		/// medianBlur - filtr medianowy 
		medianBlur(frame_gray, frame_gray, 15);
		/// imshow - pokazanie obrazu wideo
		imshow("Wynik", frame_gray);
		cvtColor(frame, frame, CV_BGR2HSV);
		resize(frame, frame, cv::Size(), 0.5, 0.5);
		imshow("Wideo", frame);
		if (cv::waitKey(30) == 27)
		{
			cout << "Klawisz esc zostal nacisniety przez uzytkownika" << endl;
			break;
		}
	}
	return 0;
}
