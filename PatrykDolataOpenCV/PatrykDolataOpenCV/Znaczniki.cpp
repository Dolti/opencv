#include "Znaczniki.h"

#define WRAZLIWOSC 10

CJoints::CJoints()
{
	///konstruktor klasy CJoints - inicjalizacja zmiennych
	if ( !InitKamery() )
	{
		cout << "Nie mozna uruchomic kamery" << endl;
		return;
	}

	for (int i = 0; i < 3; i++) m_iZnacznikBiodroHSV[i] = 0;
	for (int i = 0; i < 3; i++) m_iZnacznikKolanoHSV[i] = 0;
	for (int i = 0; i < 3; i++) m_iZnacznikKostkaHSV[i] = 0;

	m_iBiodroKolanoMilimetry = 0;
	m_iKolanoKostkaMilimetry = 0;
	m_iZnacznikHSV = 0;
	m_iZmierzone = 0;
	m_dPredkoscKostka = 0.0;
	m_dPrzyspieszenieKostka = 0.0;
	m_dDroga = 0.0;
	m_dSkala = 0.0;
	m_pWspZnacznikaNaBiodrze.x = 0;
	m_pWspZnacznikaNaKolanie.x = 0;
	m_pWspZnacznikaNaKostce.x = 0;
	m_pWspZnacznikaNaBiodrze.y = 0;
	m_pWspZnacznikaNaKolanie.y= 0;
	m_pWspZnacznikaNaKostce.y = 0;
	m_pWspZnacznikaNaKostceTMP.x = 0;
	m_pWspZnacznikaNaKostceTMP.y = 0;
	m_pWspZnacznikaNaKostceStart.x = 0;
	m_pWspZnacznikaNaKostceStart.y = 0;

	m_pWspZnacznikaNaKolanieTMP.x = 0;
	m_pWspZnacznikaNaKolanieTMP.y = 0;
	m_pWspZnacznikaNaKolanieStart.x = 0;
	m_pWspZnacznikaNaKolanieStart.y = 0;

	m_pWspZnacznikaNaBiodrzeTMP.x = 0;
	m_pWspZnacznikaNaBiodrzeTMP.y = 0;
	m_pWspZnacznikaNaBiodrzeStart.x = 0;
	m_pWspZnacznikaNaBiodrzeStart.y = 0;
}

void CJoints::WystartujWatek()
{
	///Startuje watek odpowiedzialny za obliczanie predkosci i przyspieszen
	m_tWatekKostka = thread(&CJoints::Watek, this);
	m_tWatekKolano = thread(&CJoints::Watek1, this);
	m_tWatekBiodro = thread(&CJoints::Watek2, this);
}

void CJoints::Watek()
{
	///Metoda oblicza predkosc oraz przyspieszenie co 100 ms
	while (true)
	{
		int iPowtorzenia = 0;
		double dCzas = 0.0;
		double dPredkoscTMP = 0.0;
		PoliczSkale();
		m_pWspZnacznikaNaKostceStart = m_pWspZnacznikaNaKostceTMP;
		while ((m_pWspZnacznikaNaKostceTMP.x > (m_pWspZnacznikaNaKostce.x + WRAZLIWOSC)) ||
			(m_pWspZnacznikaNaKostceTMP.x < (m_pWspZnacznikaNaKostce.x - WRAZLIWOSC)) ||
			(m_pWspZnacznikaNaKostceTMP.y > (m_pWspZnacznikaNaKostce.y + WRAZLIWOSC)) ||
			(m_pWspZnacznikaNaKostceTMP.y < (m_pWspZnacznikaNaKostce.y - WRAZLIWOSC)))
		{
			iPowtorzenia++;
			dCzas = iPowtorzenia*100;
			dCzas = dCzas / 1000;
			m_dPredkoscKostka = PoliczPredkosc(m_pWspZnacznikaNaKostceStart, m_pWspZnacznikaNaKostce, dCzas);
			m_dPrzyspieszenieKostka = PoliczPrzyspieszenie(dPredkoscTMP, m_dPredkoscKostka);
			dPredkoscTMP = m_dPredkoscKostka;
			Sleep(100);
		}
		m_dPrzyspieszenieKostka = 0.0;
		m_dPredkoscKostka = 0.0;
	}
}
void CJoints::Watek1()
{
	///Metoda oblicza predkosc oraz przyspieszenie co 100 ms
	while (true)
	{
		int iPowtorzenia = 0;
		double dCzas = 0.0;
		double dPredkoscTMP = 0.0;
		PoliczSkale();
		m_pWspZnacznikaNaKolanieStart = m_pWspZnacznikaNaKolanieTMP;
		while ((m_pWspZnacznikaNaKolanieTMP.x > (m_pWspZnacznikaNaKolanie.x + WRAZLIWOSC)) ||
			(m_pWspZnacznikaNaKolanieTMP.x < (m_pWspZnacznikaNaKolanie.x - WRAZLIWOSC)) ||
			(m_pWspZnacznikaNaKolanieTMP.y >(m_pWspZnacznikaNaKolanie.y + WRAZLIWOSC)) ||
			(m_pWspZnacznikaNaKolanieTMP.y < (m_pWspZnacznikaNaKolanie.y - WRAZLIWOSC)))
		{
			iPowtorzenia++;
			dCzas = iPowtorzenia * 100;
			dCzas = dCzas / 1000;
			m_dPredkoscKolano = PoliczPredkosc(m_pWspZnacznikaNaKolanieStart, m_pWspZnacznikaNaKolanie, dCzas);
			m_dPrzyspieszenieKolano = PoliczPrzyspieszenie(dPredkoscTMP, m_dPredkoscKostka);
			dPredkoscTMP = m_dPredkoscKolano;
			Sleep(100);
		}
		m_dPrzyspieszenieKolano = 0.0;
		m_dPredkoscKolano = 0.0;
	}
}
void CJoints::Watek2()
{
	///Metoda oblicza predkosc oraz przyspieszenie co 100 ms
	while (true)
	{
		int iPowtorzenia = 0;
		double dCzas = 0.0;
		double dPredkoscTMP = 0.0;
		PoliczSkale();
		m_pWspZnacznikaNaBiodrzeStart = m_pWspZnacznikaNaBiodrzeTMP;
		while ((m_pWspZnacznikaNaBiodrzeTMP.x > (m_pWspZnacznikaNaBiodrze.x + WRAZLIWOSC)) ||
			(m_pWspZnacznikaNaBiodrzeTMP.x < (m_pWspZnacznikaNaBiodrze.x - WRAZLIWOSC)) ||
			(m_pWspZnacznikaNaBiodrzeTMP.y >(m_pWspZnacznikaNaBiodrze.y + WRAZLIWOSC)) ||
			(m_pWspZnacznikaNaBiodrzeTMP.y < (m_pWspZnacznikaNaBiodrze.y - WRAZLIWOSC)))
		{
			iPowtorzenia++;
			dCzas = iPowtorzenia * 100;
			dCzas = dCzas / 1000;
			m_dPredkoscBiodro = PoliczPredkosc(m_pWspZnacznikaNaBiodrzeStart, m_pWspZnacznikaNaBiodrze, dCzas);
			m_dPrzyspieszenieBiodro = PoliczPrzyspieszenie(dPredkoscTMP, m_dPredkoscBiodro);
			dPredkoscTMP = m_dPredkoscBiodro;
			Sleep(100);
		}
		m_dPrzyspieszenieBiodro = 0.0;
		m_dPredkoscBiodro = 0.0;
	}
}

void CJoints::WyswietlDaneNaEkranie(cv::Mat imgOryginalny)
{
	///Wyswietlenie pozycji/predkosci/przyspieszenia w oknie "Obraz oryginalny"
	string box_text = cv::format("[%d,%d]", m_pWspZnacznikaNaBiodrze.x, m_pWspZnacznikaNaBiodrze.y);
	string box_przyspieszenie = cv::format("Przyspieszenie => %f", m_dPrzyspieszenieKostka);
	string box_predkosc = cv::format("Predkosc => %f", m_dPredkoscKostka);

	putText(imgOryginalny, box_text, cv::Point(m_pWspZnacznikaNaBiodrze.x, m_pWspZnacznikaNaBiodrze.y), cv::FONT_HERSHEY_PLAIN, 1.0, CV_RGB(0, 255, 0), 2.0);
	string box_text1 = cv::format("[%d,%d]", m_pWspZnacznikaNaKolanie.x, m_pWspZnacznikaNaKolanie.y);
	putText(imgOryginalny, box_text1, cv::Point(m_pWspZnacznikaNaKolanie.x, m_pWspZnacznikaNaKolanie.y), cv::FONT_HERSHEY_PLAIN, 1.0, CV_RGB(0, 255, 0), 2.0);
	string box_text2 = cv::format("[%d,%d]", m_pWspZnacznikaNaKostce.x, m_pWspZnacznikaNaKostce.y);
	putText(imgOryginalny, box_text2, cv::Point(m_pWspZnacznikaNaKostce.x, m_pWspZnacznikaNaKostce.y), cv::FONT_HERSHEY_PLAIN, 1.0, CV_RGB(0, 255, 0), 2.0);
	putText(imgOryginalny, box_predkosc, cv::Point(25, 25), cv::FONT_HERSHEY_PLAIN, 1.0, CV_RGB(0, 255, 0), 2.0);
	putText(imgOryginalny, box_przyspieszenie, cv::Point(25, 50), cv::FONT_HERSHEY_PLAIN, 1.0, CV_RGB(0, 255, 0), 2.0);
	if (m_dPredkoscKostka >= 0.0) m_sPomiaryKostka << m_dPredkoscKostka << " " << m_dPrzyspieszenieKostka << " " << box_text2<< "\n";
	if (m_dPredkoscKolano >= 0.0) m_sPomiaryKolano << m_dPredkoscKolano << " " << m_dPrzyspieszenieKolano << " " << box_text1 << "\n";
	if (m_dPredkoscBiodro >= 0.0) m_sPomiaryBiodro << m_dPredkoscBiodro << " " << m_dPrzyspieszenieBiodro << " " << box_text << "\n";

	
	imshow("Oryginalny obraz", imgOryginalny);
}

void CJoints::Tic()
{
	///Odpowiednik funckcji z Matlaba - start odliczania
	tictoc_stack.push(clock());
}

double CJoints::Toc()
{
	///Odpowiednik funckcji z Matlaba - koniec odliczania, zwraca czas ktory uplynal
	double dCzas = ((double)(clock() - tictoc_stack.top())) / CLOCKS_PER_SEC;
	tictoc_stack.pop();
	
	return dCzas;
}

void CJoints::PoliczSkale()
{
	///Przelicza skale - zamienia pixele na milimetry dzieki znanej odleglosci miedzy znacznikiem na kolanie, a na kostce
	int iX1, iX2, iY1, iY2;
	double dPiksele = 0.0;

	iX1 = abs(m_pWspZnacznikaNaKostce.x - m_pWspZnacznikaNaKolanie.x);
	iY1 = abs(m_pWspZnacznikaNaKostce.y - m_pWspZnacznikaNaKolanie.y);
	iX2 = iX1*iX1;
	iY2 = iY1*iY1;
	dPiksele = sqrt(iX2 + iY2);
	m_dSkala = m_iKolanoKostkaMilimetry / dPiksele;
}

double CJoints::PoliczPrzyspieszenie(double dPredkoscTMP, double dPredkosc)
{
	///Liczy przyspieszenie ze wzoru na pochodna 
	///zrodlo http:///programowanie.opole.pl/archives/1277

	double dPrzyspieszenie = 0.0;
	dPrzyspieszenie = (dPredkosc - dPredkoscTMP) / 0.1;
	if (dPrzyspieszenie < 0.0 )
	{
		dPrzyspieszenie = 0.0;
	}
	return dPrzyspieszenie;
}

double CJoints::PoliczPredkosc(cv::Point Start, cv::Point Stop, double Czas)
{
	///Oblicza predkosc - ze wzoru pitagorasa oblicza odleglosc miedzy znacznikami nastepnie dzieli przez czas
	double dPredkosc = 0.0;
	double dDroga = 0.0;
	int iPrzesuniecieX = 0;
	int iPrzesuniecieY = 0;

	iPrzesuniecieX = Stop.x - Start.x;
	iPrzesuniecieY = Stop.y - Stop.y;
	iPrzesuniecieX = iPrzesuniecieX*iPrzesuniecieX;
	iPrzesuniecieY = iPrzesuniecieY*iPrzesuniecieY;
	dDroga = sqrt(iPrzesuniecieX + iPrzesuniecieY)*m_dSkala;
	dDroga = dDroga / 1000;
	if (Czas != 0.0) dPredkosc = dDroga / Czas;

	return dPredkosc;
}

bool CJoints::InitKamery()
{
	///Inicjalizacja kamery
	m_wideo = cv::VideoCapture(0);
	return m_wideo.isOpened();
}

cv::Mat CJoints::PrzetworzObraz(cv::Mat imgHSV, int iMinH, int iMaxH, int iMinS, int iMaxS, int iMinV, int iMaxV)
{
	///Przetwarza oryginalny obraz z kamery zgodnie z wartosciami na suwakach
	cv::Mat imgPrzetworzony;

	//// inRange - uwzglednienie wartosci ustawionych na suwakach
	inRange(imgHSV, cv::Scalar(iMinH, iMinS, iMinV), cv::Scalar(iMaxH, iMaxS, iMaxV), imgPrzetworzony);
	//// erode - przeprowadza erozje obrazu z uzyciem zdefiniowanego elementu strukturalnego
	erode(imgPrzetworzony, imgPrzetworzony, cv::getStructuringElement(cv::MORPH_ELLIPSE, cv::Size(5, 5)));
	//// dilate - przeprowadza dylatacje obrazu z uzyciem zdefiniowanego elementu strukturalnego
	dilate(imgPrzetworzony, imgPrzetworzony, cv::getStructuringElement(cv::MORPH_ELLIPSE, cv::Size(5, 5)));
	dilate(imgPrzetworzony, imgPrzetworzony, cv::getStructuringElement(cv::MORPH_ELLIPSE, cv::Size(5, 5)));
	erode(imgPrzetworzony, imgPrzetworzony, cv::getStructuringElement(cv::MORPH_ELLIPSE, cv::Size(5, 5)));

	imshow("Przetworzony obraz", imgPrzetworzony);

	return imgPrzetworzony;
}

bool CJoints::OkreslHSVznacznikow()
{
	///Konfiguracja znacznikow - ustawienie parametrow HSV dla kazdego znacznika
	bool bSuccess = true;
	int iWyswietlono = 0;
	cv::Mat imgThresholded;
	cv::Mat imgHSV;

	cout << "Ustaw wartosci HSV dla znacznikow, dobierz wartosci na suwakach tak, \naby w oknie \"Przetworzony obraz\" bialy byl tylko znacznik. \n" << endl;

	//// cv::namedWindow - nazwa nowego okna
	cv::namedWindow("Konfiguracja", CV_WINDOW_AUTOSIZE);
	//// iMinH, iMaxH ... - zakresy parametrow modelu HSV
	int iMinH = 0;
	int iMaxH = 179;
	int iMinS = 0;
	int iMaxS = 255;
	int iMinV = 0;
	int iMaxV = 255;

	//// cvCreateTrackbar - tworzy suwak
	cvCreateTrackbar("Min H", "Konfiguracja", &iMinH, 179);
	cvCreateTrackbar("Max H", "Konfiguracja", &iMaxH, 179);
	cvCreateTrackbar("Min S", "Konfiguracja", &iMinS, 255);
	cvCreateTrackbar("Max S", "Konfiguracja", &iMaxS, 255);
	cvCreateTrackbar("Min V", "Konfiguracja", &iMinV, 255);
	cvCreateTrackbar("Max V", "Konfiguracja", &iMaxV, 255);
	
	while ( m_iZnacznikHSV < 3 )
	{
		cv::Mat imgOryginalny;
		cv::Mat imgHSV;

		if (!m_wideo.read(imgOryginalny))
		{
			cout << "Nie mozna odczytac klatki z kamery" << endl;
			break;
		}
		imshow("Oryginalny obraz", imgOryginalny);
		//// cvtColor - konwersja koloru z BGR do HSV
		cvtColor(imgOryginalny, imgHSV, cv::COLOR_BGR2HSV);
		switch (m_iZnacznikHSV)
		{
		case 0:
			if (iWyswietlono == 0)
			{
				cout << ">>Znacznik na biodrze. Nacisnij spacje by przejsc do nastepnego znacznika." << endl;
				iWyswietlono = 1;
			}
			m_iZnacznikBiodroHSV[0] = iMinH;
			m_iZnacznikBiodroHSV[1] = iMaxH;
			m_iZnacznikBiodroHSV[2] = iMinS;
			m_iZnacznikBiodroHSV[3] = iMaxS;
			m_iZnacznikBiodroHSV[4] = iMinV;
			m_iZnacznikBiodroHSV[5] = iMaxV;
			PrzetworzObraz(imgHSV, m_iZnacznikBiodroHSV[0], m_iZnacznikBiodroHSV[1], m_iZnacznikBiodroHSV[2], m_iZnacznikBiodroHSV[3], m_iZnacznikBiodroHSV[4], m_iZnacznikBiodroHSV[5]);
			break;
		case 1:
			if (iWyswietlono == 0)
			{
				cout << ">>Znacznik na kolanie. Nacisnij spacje by przejsc do nastepnego znacznika." << endl;
				iWyswietlono = 1;
			}
			m_iZnacznikKolanoHSV[0] = iMinH;
			m_iZnacznikKolanoHSV[1] = iMaxH;
			m_iZnacznikKolanoHSV[2] = iMinS;
			m_iZnacznikKolanoHSV[3] = iMaxS;
			m_iZnacznikKolanoHSV[4] = iMinV;
			m_iZnacznikKolanoHSV[5] = iMaxV;
			PrzetworzObraz(imgHSV, m_iZnacznikKolanoHSV[0], m_iZnacznikKolanoHSV[1], m_iZnacznikKolanoHSV[2], m_iZnacznikKolanoHSV[3], m_iZnacznikKolanoHSV[4], m_iZnacznikKolanoHSV[5]);
			break;
		case 2:
			if (iWyswietlono == 0)
			{
				cout << ">>Znacznik na kostce. Nacisnij spacje by przejsc dalej." << endl;
				iWyswietlono = 1;
			}
			m_iZnacznikKostkaHSV[0] = iMinH;
			m_iZnacznikKostkaHSV[1] = iMaxH;
			m_iZnacznikKostkaHSV[2] = iMinS;
			m_iZnacznikKostkaHSV[3] = iMaxS;
			m_iZnacznikKostkaHSV[4] = iMinV;
			m_iZnacznikKostkaHSV[5] = iMaxV;
			PrzetworzObraz(imgHSV, m_iZnacznikKostkaHSV[0], m_iZnacznikKostkaHSV[1], m_iZnacznikKostkaHSV[2], m_iZnacznikKostkaHSV[3], m_iZnacznikKostkaHSV[4], m_iZnacznikKostkaHSV[5]);
			break;
		default:
			break;
		}

		if ( cv::waitKey(30) == 32 )
		{
			m_iZnacznikHSV++;
			iWyswietlono = 0;
		}
	}
	cv::destroyWindow("Konfiguracja");
	cv::destroyWindow("Przetworzony obraz");
	cv::destroyWindow("Oryginalny obraz");
	if (m_iZnacznikHSV != 3) bSuccess = false;
	m_iZnacznikHSV = 0;

	return bSuccess;
}

void CJoints::SledzenieNogi()
{
	///Okreslenie pozycji kazdego ze znacznikow, rysowanie lini ktore je lacza
	cv::Mat imgPrzetworzony;
	cv::Mat imgOryginalny;
	cv::Mat imgHSV;

	if (m_wideo.read(imgOryginalny))
	{
		cv::Mat imgLines = cv::Mat::zeros(imgOryginalny.size(), CV_8UC3);
		//// cvtColor - konwersja koloru z BGR do HSV
		cvtColor(imgOryginalny, imgHSV, cv::COLOR_BGR2HSV);
		imgPrzetworzony = PrzetworzObraz(imgHSV, m_iZnacznikBiodroHSV[0], m_iZnacznikBiodroHSV[1], m_iZnacznikBiodroHSV[2], m_iZnacznikBiodroHSV[3], m_iZnacznikBiodroHSV[4], m_iZnacznikBiodroHSV[5]);
		m_pWspZnacznikaNaBiodrze = WykryjZnaczniki(imgPrzetworzony, m_pWspZnacznikaNaBiodrze);
		imgPrzetworzony = PrzetworzObraz(imgHSV, m_iZnacznikKolanoHSV[0], m_iZnacznikKolanoHSV[1], m_iZnacznikKolanoHSV[2], m_iZnacznikKolanoHSV[3], m_iZnacznikKolanoHSV[4], m_iZnacznikKolanoHSV[5]);
		m_pWspZnacznikaNaKolanie = WykryjZnaczniki(imgPrzetworzony, m_pWspZnacznikaNaKolanie);
		imgPrzetworzony = PrzetworzObraz(imgHSV, m_iZnacznikKostkaHSV[0], m_iZnacznikKostkaHSV[1], m_iZnacznikKostkaHSV[2], m_iZnacznikKostkaHSV[3], m_iZnacznikKostkaHSV[4], m_iZnacznikKostkaHSV[5]);
		m_pWspZnacznikaNaKostceTMP = m_pWspZnacznikaNaKostce;
		m_pWspZnacznikaNaKolanieTMP = m_pWspZnacznikaNaKolanie;
		m_pWspZnacznikaNaBiodrzeTMP = m_pWspZnacznikaNaBiodrze;
		m_pWspZnacznikaNaKostce = WykryjZnaczniki(imgPrzetworzony, m_pWspZnacznikaNaKostce);
		//rysowanie lini
		line(imgLines, cv::Point(m_pWspZnacznikaNaBiodrze.x, m_pWspZnacznikaNaBiodrze.y), cv::Point(m_pWspZnacznikaNaKolanie.x, m_pWspZnacznikaNaKolanie.y), cv::Scalar(0, 0, 255), 2);
		line(imgLines, cv::Point(m_pWspZnacznikaNaKolanie.x, m_pWspZnacznikaNaKolanie.y), cv::Point(m_pWspZnacznikaNaKostce.x, m_pWspZnacznikaNaKostce.y), cv::Scalar(0, 255, 0), 2);
		imgOryginalny = imgOryginalny + imgLines;
		imshow("Oryginalny obraz", imgOryginalny);
		imgLines = cv::Mat::zeros(imgOryginalny.size(), CV_8UC3);
		WyswietlDaneNaEkranie(imgOryginalny);
	}
}

bool CJoints::PodajWymiaryNogi()
{
	///Pobranie odleglosci znacznika na kolanie do znacznika na kostce - potrzebne do wyliczenia skali
	bool bSuccess = true;
	cout << "Podaj odleglosc miedzy znacznikem na kolanie, a znacznikiem na kostce w mm:" << endl;
	cin >> m_iKolanoKostkaMilimetry;
	if (m_iKolanoKostkaMilimetry < 10 || m_iKolanoKostkaMilimetry > 1000) bSuccess = false;

	return bSuccess;
}

cv::Point CJoints::WykryjZnaczniki(cv::Mat imgPrzetworzonyObraz, cv::Point wspolrzedne)
{
	///Wykrywa znaczniki korzystajac moment
	cv::Moments oMoments = moments(imgPrzetworzonyObraz);
	double dM01 = oMoments.m01;
	double dM10 = oMoments.m10;
	double dArea = oMoments.m00;
	if (dArea > 10000)
	{
		wspolrzedne.x = dM10 / dArea;
		wspolrzedne.y = dM01 / dArea;
	}
	
	return wspolrzedne;
}

void CJoints::ZapiszDaneDoPliku()
{
	///Zapisuje dane do pliku pomiary.txt
	ofstream plik, plik2, plik3;
	plik.open("pomiaryKostka.txt");
	if (plik.is_open()){
		plik << m_sPomiaryKostka.str();
		plik.close();
	}
	plik2.open("pomiaryKolano.txt");
	if (plik2.is_open()){
		plik2 << m_sPomiaryKolano.str();
		plik2.close();
	}
	plik3.open("pomiaryBiodro.txt");
	if (plik3.is_open()){
		plik3 << m_sPomiaryBiodro.str();
		plik3.close();
	}
}

CJoints::~CJoints()
{
}